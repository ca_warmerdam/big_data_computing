# Distributed Computing Assignment (Assignment 2)

The given file is split into ten parts that can be processed by one or more nodes in a cluster

```
usage: script.py [-h] [-n NUMBER_OF_PROCESSES] [--host_ip HOST_IP]
                 fastq_file [output_file]
script.py: error: the following arguments are required: fastq_file
```

- To start the host process, don't input the number of processes or the host ip. like so:
`script.py /commons/Themas/Thema12/HPC/rnaseq.fastq`

The `-n` option is currently reserved for nodes only and will have no effect on the host process.

- To start nodes, the ip of the host computer is required as well as the fastq_file. like so:
`script.py /commons/Themas/Thema12/HPC/rnaseq.fastq --host_ip bin254`

When a node will only start 1 process by default. the `-n` option can be used to start another number of processes