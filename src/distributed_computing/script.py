#!/usr/bin/env python3
"""Calculates the average PHRED score per base for a fastq file"""
import sys
import os
import argparse
import queue
import collections
import time
import multiprocessing as mp
import multiprocessing.managers as managers

__author__ = "Robert Warmerdam"

NODELIST = ["bin254", "bin255", "bin256", "bin257"]
PORTNUM = 5000
AUTHKEY = b'distributed_fastq_quality_scoring'


class DistributedFastqFileProcessor:
    def __init__(self, fastq_file_path, nodelist, n_processes = 10):
        self.number_of_processes = n_processes
        self.nodelist = nodelist
        self.fastq_file_path = fastq_file_path
        self.sum_of_quality_scores_per_base = collections.Counter()
        self.total_bases_per_position = collections.Counter()
        self.quality_scores_per_base = dict()

    def process_fastq_file(self):
        """
        Processes a fastq file: average PHRED scores per base number are calculated
        """
        # Start a shared manager server and access its queues
        manager = make_server_manager(PORTNUM, AUTHKEY)
        shared_job_q = manager.get_job_queue()
        shared_result_q = manager.get_result_queue()
        # Prepare processes
        self.prepare_jobs(shared_job_q)
        # Wait until all results are ready in shared_result_q
        num_results = 0
        while num_results < self.number_of_processes:
            out_tuple = shared_result_q.get()
            sum_of_quality_scores_per_base, total_bases_per_position = out_tuple
            self.sum_of_quality_scores_per_base += sum_of_quality_scores_per_base
            self.total_bases_per_position += total_bases_per_position
            num_results += 1
        # Sleep a bit before shutting down the server - to give clients time to
        # realize the job queue is empty and exit in an orderly way.
        time.sleep(2)
        manager.shutdown()
        self.calculate_output()

    def prepare_jobs(self, shared_job_queue):
        """
        Prepares the different jobs for reading a file.
        The different jobs get a starting byte, an ending byte and the arguments for storing end results
        :param shared_job_queue: the job queue shared among workers
        """
        # Get size of fastq_file in bytes
        size = os.path.getsize(self.fastq_file_path)
        # Get the size for each daughter process by dividing
        # the total size by the number of processes
        size_per_daughter_process = size / self.number_of_processes
        # Initialize starting and ending byte
        starting_byte = 0
        ending_byte = size_per_daughter_process
        for i in range(self.number_of_processes):
            # Initialize process with rounded byte integers
            shared_job_queue.put((self.fastq_file_path, round(starting_byte), round(ending_byte)))
            # For next processes: update starting and ending byte
            starting_byte = ending_byte
            ending_byte += size_per_daughter_process

    def calculate_output(self):
        """
        Calculate the output of the processing step by dividing the sum of quality scores per base by the number of
        bases for that base number
        """
        for i, n_bases in self.total_bases_per_position.items():
            self.quality_scores_per_base[i] = self.sum_of_quality_scores_per_base[i] / n_bases

    def write_output(self, opened_output_file):
        """
        Write the output to a file
        :param opened_output_file: an opened output file
        """
        opened_output_file.write("base_nr, average_PHRED\n")
        for base_nr, score in self.quality_scores_per_base.items():
            opened_output_file.write("{}, {}\n".format(base_nr + 1, score))


class FastqFilePartProcessor:
    def __init__(self, fastq_file_path):
        self.fastq_file_path = fastq_file_path

    def process_fastq_part(self, starting_byte, ending_byte, sum_of_quality_scores_per_base, total_bases_per_position):
        """
        Process a part of a fastq file
        The total score per base number is calculated for this part as well as the total number of bases for each
        base number
        :param starting_byte: The byte where the process will start
        :param ending_byte: The byte where the process doing the next chunk will start (or end of file)
        :param sum_of_quality_scores_per_base: The total score per base number
        :param total_bases_per_position: The total number of bases for each base number
        """
        # Open the fastq_file
        with open(self.fastq_file_path) as opened_fastq_file:
            # Go te the right location in the file using seek (unit is byte)
            opened_fastq_file.seek(starting_byte)
            # Finish the current line
            byte_position_upstream_of_seq_id_line = starting_byte
            line = opened_fastq_file.readline()
            # While the line is not a sequence identifier, read another line
            while not self.is_line_sequence_identifier(line):
                byte_position_upstream_of_seq_id_line = opened_fastq_file.tell()
                line = opened_fastq_file.readline()
            # Return to the beginning of the seq id line
            opened_fastq_file.seek(byte_position_upstream_of_seq_id_line)
            # Read reads until the read has finished that ends after the ending byte
            while opened_fastq_file.tell() < ending_byte:
                # Read a read
                self.read_a_read(opened_fastq_file, sum_of_quality_scores_per_base, total_bases_per_position)

    def read_a_read(self, opened_fastq_file, sum_of_quality_scores_per_base, total_bases_per_position):
        """
        Process a single read by adding scores to the total quality score of each base number and counting the total
        bases per base number
        :param opened_fastq_file: an opened fastq file
        :param sum_of_quality_scores_per_base: a list containing quality scores in integers
        :param total_bases_per_position: a list containing the count of bases in integers
        """
        # Read seq_id line
        opened_fastq_file.readline()
        # Read sequence line (newline present)
        sequence_line = opened_fastq_file.readline()
        # Read strand line
        opened_fastq_file.readline()
        # Read quality line (newline present)
        quality_line = opened_fastq_file.readline()
        # Check if the read is valid (sequence_line length equal to quality line length)
        if not self.is_valid_read(sequence_line, quality_line):
            # Forget about the read if it is messed up
            return
        # Strip newline characters
        quality_line = quality_line.rstrip()
        # Process the quality line by adding the score for each base nr
        # and add 1 to the total bases list for each present base nr
        for i, char in enumerate(quality_line):
            sum_of_quality_scores_per_base[i] += (ord(char) - 33)
            total_bases_per_position[i] += 1

    @staticmethod
    def is_valid_read(sequence_line, quality_line):

        """
        Determines if the read is valid
        The length of the sequence and the quality line should be equal and above zero
        :param sequence_line: A string containing the line of a sequence with newline character
        :param quality_line: A string containing the line with quality characters with newline character
        :return: True if the read is valid
        """
        # length of quality(3)/seq(1) should match
        return len(sequence_line) == len(quality_line) and len(quality_line) > 0

    @staticmethod
    def is_line_sequence_identifier(line):
        """
        Checks if the line is a seq id line
        :param line: A possible seq id line
        :return: True if the line is a seq id line
        """
        return line.startswith('@') and ' ' in line


class ArgumentParser:
    @classmethod
    def parse(cls, argv):
        """Parses commandline arguments"""
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument('fastq_file', type=cls.test_fastq_file, help="The path to an 'Illumina 1.3' FASTQ file")
        parser.add_argument('-n', '--number_of_processes', required=False, type=int, default=1,
                            help="The number of processes to start")
        parser.add_argument('output_file', nargs='?',
                            help="The path to a CSV output file")
        parser.add_argument('--host_ip', type=str, required=False, help="The ip of the host computer")
        args = parser.parse_args(argv)
        return args

    @classmethod
    def test_fastq_file(cls, fastq_file):
        """Raises an error if the fastq_file is not readable"""
        if not os.path.isfile(fastq_file):
            raise argparse.ArgumentTypeError('fastq file {} is not a file'.format(fastq_file))
        if not os.access(fastq_file, os.R_OK):
            raise argparse.ArgumentTypeError('fastq file {} is not readable file'.format(fastq_file))
        return fastq_file


class Client:
    def __init__(self, host_ip, host_port_number, host_process_authkey, n_local_processes):
        self.authkey = host_process_authkey
        self.portnum = host_port_number
        self.ip = host_ip
        self.n_local_processes = n_local_processes
        self.start_time = 0

    def run_client(self):
        self.start_time = time.time()
        working = True
        while working:
            try:
                manager = self.make_client_manager()
                job_q = manager.get_job_queue()
                result_q = manager.get_result_queue()
                self.mp_fastq_scorer(job_q, result_q)
                print('work is done. Aborting...')
                working = False
            except ConnectionRefusedError as e:
                print('{} seconds have passed trying to connect to {}:{}'.format(time.time() - self.start_time, self.ip,
                                                                           self.portnum))
                time.sleep(5)
            except EOFError:
                print('connection to {}:{} was closed. Aborting...'.format(self.ip, self.portnum))
                working = False

    def mp_fastq_scorer(self, shared_job_q, shared_result_q):
        """
        Split the work with jobs in shared_job_q and results in
        shared_result_q into several processes. Launch each process with
        factorizer_worker as the worker function, and wait until all are
        finished.
        """
        processes = []
        for i in range(self.n_local_processes):
            p = mp.Process(
                target=self.empty_job_queue,
                args=(shared_job_q, shared_result_q))
            processes.append(p)
            p.start()

        for p in processes:
            p.join()

    def empty_job_queue(self, job_queue, result_queue):
        """
        A worker function to be launched in a separate process. Takes jobs from
        job_q - each job a list of numbers to factorize. When the job is done,
        the result (dict mapping number -> list of factors) is placed into
        result_q. Runs until job_q is empty.
        """
        while True:
            try:
                # Get job properties
                job = job_queue.get_nowait()
                print(job)
                fastq_file_path, starting_byte, ending_byte = job
                # Construct and use lists to overcome shared mem performance penalty
                sum_of_quality_scores_per_base = collections.Counter()
                total_bases_per_position = sum_of_quality_scores_per_base.copy()
                # Get fastq file part processor
                my_file_part_processor = FastqFilePartProcessor(fastq_file_path)
                my_file_part_processor.process_fastq_part(starting_byte, ending_byte, sum_of_quality_scores_per_base,
                                                          total_bases_per_position)
                result_queue.put((sum_of_quality_scores_per_base, total_bases_per_position))
            except queue.Empty:
                return

    def make_client_manager(self):
        """
        Create a manager for a client. This manager connects to a server on the
        given address and exposes the get_job_q and get_result_q methods for
        accessing the shared queues from the server.
        Return a manager object.
        """

        class ServerQueueManager(managers.SyncManager):
            pass

        ServerQueueManager.register('get_job_queue')
        ServerQueueManager.register('get_result_queue')

        manager = ServerQueueManager(address=(self.ip, self.portnum), authkey=self.authkey)
        manager.connect()

        print('Client connected to {}:{}'.format(self.ip, self.portnum))
        return manager


def make_server_manager(port, authkey):
    """
    Create a manager for the server, listening on the given port.
    Return a manager object with get_job_q and get_result_q methods.
    """
    job_queue = mp.Queue()
    result_queue = mp.Queue()

    # This is based on the examples in the official docs of multiprocessing.
    # get_{job|result}_q return synchronized proxies for the actual Queue
    # objects.
    class JobQueueManager(managers.SyncManager):
        pass

    JobQueueManager.register('get_job_queue', callable=lambda: job_queue)
    JobQueueManager.register('get_result_queue', callable=lambda: result_queue)

    manager = JobQueueManager(address=('', port), authkey=authkey)
    manager.start()
    print('Server started at port {}'.format(port))
    return manager


def main(argv=None):
    if argv is None:
        argv = sys.argv
    args = ArgumentParser.parse(argv[1:])
    if args.host_ip:
        Client(args.host_ip, PORTNUM, AUTHKEY, args.number_of_processes).run_client()
        return 0
    # Parse Fastq file
    my_file_processor = DistributedFastqFileProcessor(args.fastq_file, NODELIST)
    my_file_processor.process_fastq_file()
    if args.output_file is not None:
        with open(args.output_file, 'w') as opened_output_file:
            my_file_processor.write_output(opened_output_file)
    else:
        my_file_processor.write_output(sys.stdout)
    return 0


if __name__ == "__main__":
    sys.exit(main())
