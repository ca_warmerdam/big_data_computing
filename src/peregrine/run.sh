#!/bin/bash
#SBATCH --time 0:20:00
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=4096
#SBATCH --partition=short
#SBATCH --output=out.txt

module load Python/3.6.4-foss-2018a

/usr/bin/env python3 script.py -n 4 ../rnaseq.fastq out.csv
