#!/usr/bin/env python3
"""Calculates the average PHRED score per base for a fastq file"""
import sys
import os
import argparse
import multiprocessing as mp
import queue

__author__ = "Robert Warmerdam"


class MultiProcessingFastqFileProcessor:
    def __init__(self, fastq_file_path, n_cpu=1):
        self.fastq_file_path = fastq_file_path
        self.n_cpu = n_cpu
        # Prepare list to store processes in
        self.processes = list()
        self.sum_of_quality_scores_per_base = dict()
        self.total_bases_per_position = dict()
        self.quality_scores_per_base = dict()

    def process_fastq_file(self):
        """
        Processes a fastq file: average PHRED scores per base number are calculated
        """
        # Get size of fastq_file in bytes
        size = os.path.getsize(self.fastq_file_path)
        # Get the size for each daughter process by dividing
        # the total size by the number of processes
        size_per_daughter_process = size / self.n_cpu
        # Initialize starting and ending byte
        starting_byte = 0
        ending_byte = size_per_daughter_process
        # Prepare thread save arrays for storing summed quality scores and total bases per base_nr
        first_read_length = self.get_first_read_length()
        quality_sum_array = mp.Array('l', first_read_length)
        total_bases_array = mp.Array('l', first_read_length)
        too_long_queue = mp.Queue()
        # Prepare processes
        self.prepare_processes(starting_byte, ending_byte, size_per_daughter_process,
                               (quality_sum_array, total_bases_array, too_long_queue))
        self.start_processes()
        self.join_processes()
        # Construct dictionaries from thread save arrays to facilitate longer reads
        self.sum_of_quality_scores_per_base = {i: x for i, x in enumerate(quality_sum_array)}
        self.total_bases_per_position = {i: x for i, x in enumerate(total_bases_array)}
        # Process longer reads
        self.process_long_reads(too_long_queue)
        self.calculate_output()

    def join_processes(self):
        """
        Join parallel processes
        """
        for p in self.processes:
            p.join()

    def start_processes(self):
        """
        Start parallel Processes
        """
        for p in self.processes:
            p.start()

    def prepare_processes(self, starting_byte, ending_byte, size_per_daughter_process,
                          storage_args):
        """
        Prepares the different processes for reading a file.
        The different processes get a starting byte, an ending byte and the arguments for storing end results
        :param starting_byte: The byte where the process will start
        :param ending_byte: The byte where the process doing the next chunk will start (or end of file)
        :param size_per_daughter_process: (float) the total nr of bytes divided by the number of processes
        :param storage_args: A tuple containing items for storing end results
        """
        for i in range(self.n_cpu):
            # Initialize process with rounded byte integers
            self.processes.append(
                mp.Process(target=self.process_fastq_part, args=(round(starting_byte), round(ending_byte),
                                                                 *storage_args)))
            # For next processes: update starting and ending byte
            starting_byte = ending_byte
            ending_byte += size_per_daughter_process

    def process_long_reads(self, too_long_queue):
        """
        Process reads too long for initial scoring
        :param too_long_queue: A queue instance containing quality lines from reads
        """
        # Loop through every queue element
        while not too_long_queue.empty():
            try:
                # Process the quality line by adding the score and 1 for each base nr in the corresponding dict items
                quality_line = too_long_queue.get()
                for i, char in enumerate(quality_line):
                    self.sum_of_quality_scores_per_base[i] += (ord(char) - 33)
                    self.total_bases_per_position[i] += 1
            except queue.Empty:
                # if the queue suddenly throws an exception: try again
                pass

    def process_fastq_part(self, starting_byte, ending_byte, quality_sum_array, total_bases_array, too_long_queue):
        """
        Process a part of a fastq file
        The total score per base number is calculated for this part as well as the total number of bases for each
        base number
        :param starting_byte: The byte where the process will start
        :param ending_byte: The byte where the process doing the next chunk will start (or end of file)
        :param quality_sum_array: The total score per base number
        :param total_bases_array: The total number of bases for each base number
        :param too_long_queue: A queue instance containing quality lines from reads
        """
        # Construct and use lists to overcome shared mem performance penalty
        np_total_bases_array = [0 for i in range(len(quality_sum_array))]
        np_quality_sum_array = list(np_total_bases_array)
        # Open the fastq_file
        with open(self.fastq_file_path) as opened_fastq_file:
            # Go te the right location in the file using seek (unit is byte)
            opened_fastq_file.seek(starting_byte)
            # Finish the current line
            byte_position_upstream_of_seq_id_line = starting_byte
            line = opened_fastq_file.readline()
            # While the line is not a sequence identifier, read another line
            while not self.is_line_sequence_identifier(line):
                byte_position_upstream_of_seq_id_line = opened_fastq_file.tell()
                line = opened_fastq_file.readline()
            # Return to the beginning of the seq id line
            opened_fastq_file.seek(byte_position_upstream_of_seq_id_line)
            # Read reads until the read has finished that ends after the ending byte
            while opened_fastq_file.tell() < ending_byte:
                # Read a read
                self.read_a_read(opened_fastq_file, np_quality_sum_array, np_total_bases_array, too_long_queue)
        # Update thread save arrays for this function call
        for i, score_sum in enumerate(np_quality_sum_array):
            quality_sum_array[i] += score_sum
            total_bases_array[i] += np_total_bases_array[i]

    @staticmethod
    def is_valid_read(sequence_line, quality_line):

        """
        Determines if the read is valid
        The length of the sequence and the quality line should be equal and above zero
        :param sequence_line: A string containing the line of a sequence with newline character
        :param quality_line: A string containing the line with quality characters with newline character
        :return: True if the read is valid
        """
        # length of quality(3)/seq(1) should match
        return len(sequence_line) == len(quality_line) and len(quality_line) > 0

    @staticmethod
    def is_line_sequence_identifier(line):
        """
        Checks if the line is a seq id line
        :param line: A possible seq id line
        :return: True if the line is a seq id line
        """
        return line.startswith('@') and ' ' in line

    def read_a_read(self, opened_fastq_file, quality_sum_array, total_bases_array, too_long_queue):
        """
        Process a single read by adding scores to the total quality score of each base number and counting the total
        bases per base number
        :param opened_fastq_file: an openede fastq file
        :param quality_sum_array: a list containing quality scores in integers
        :param total_bases_array: a list containing the count of bases in integers
        :param too_long_queue: a queue instance containing read quality lines that don't fit in the numpy arrays
        """
        # Read seq_id line
        opened_fastq_file.readline()
        # Read sequence line (newline present)
        sequence_line = opened_fastq_file.readline()
        # Read strand line
        opened_fastq_file.readline()
        # Read quality line (newline present)
        quality_line = opened_fastq_file.readline()
        # Check if the read is valid (sequence_line length equal to quality line length)
        if not self.is_valid_read(sequence_line, quality_line):
            # Forget about the read if it is messed up
            return
        # Strip newline characters
        quality_line = quality_line.rstrip()
        # Check if this line fits in the default numpy array
        if len(quality_line) > len(quality_sum_array):
            # If it doesn't fit: put it in a queue that will be emptied my the main process
            too_long_queue.put(quality_line)
        else:
            # If it does fit: process the quality line by adding the score for each base nr
            # and add 1 to the total bases list for each present base nr
            for i, char in enumerate(quality_line):
                quality_sum_array[i] += (ord(char) - 33)
                total_bases_array[i] += 1

    def calculate_output(self):
        """
        Calculate the output of the processing step by dividing the sum of quality scores per base by the number of
        bases for that base number
        """
        for i, n_bases in self.total_bases_per_position.items():
            self.quality_scores_per_base[i] = self.sum_of_quality_scores_per_base[i] / n_bases

    def write_output(self, opened_output_file):
        """
        Write the output to a file
        :param opened_output_file: an opened output file
        """
        opened_output_file.write("base_nr, average_PHRED\n")
        for base_nr, score in self.quality_scores_per_base.items():
            opened_output_file.write("{}, {}\n".format(base_nr+1, score))

    def get_first_read_length(self):
        """
        Determine length of the first read in the fastq file
        :return:
        """
        length = 0
        # Open the fastq_file
        with open(self.fastq_file_path) as opened_fastq_file:
            # Read seq_id line
            opened_fastq_file.readline()
            # Read sequence line (newline present)
            sequence_line = opened_fastq_file.readline().rstrip()
            length = len(sequence_line)
        return length


class ArgumentParser:
    @classmethod
    def parse(cls, argv):
        """Parses commandline arguments"""
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument('fastq_file', type=cls.test_fastq_file, help="The path to an 'Illumina 1.3' FASTQ file")
        parser.add_argument('-n', '--number_of_processes', required=False, type=int,
                            help="The number of processes to start")
        parser.add_argument('output_file', nargs='?',
                            help="The path to a CSV output file")
        args = parser.parse_args(argv)
        return args

    @classmethod
    def test_fastq_file(cls, fastq_file):
        """Raises an error if the fastq_file is not readable"""
        if not os.path.isfile(fastq_file):
            raise argparse.ArgumentTypeError('fastq file {} is not a file'.format(fastq_file))
        if not os.access(fastq_file, os.R_OK):
            raise argparse.ArgumentTypeError('fastq file {} is not readable file'.format(fastq_file))
        return fastq_file


def main(argv=None):
    if argv is None:
        argv = sys.argv
    args = ArgumentParser.parse(argv[1:])
    # Parse Fastq file
    my_file_processor = MultiProcessingFastqFileProcessor(args.fastq_file, n_cpu=args.number_of_processes)
    my_file_processor.process_fastq_file()
    if args.output_file is not None:
        with open(args.output_file, 'w') as opened_output_file:
            my_file_processor.write_output(opened_output_file)
    else:
        my_file_processor.write_output(sys.stdout)
    return 0


if __name__ == "__main__":
    sys.exit(main())