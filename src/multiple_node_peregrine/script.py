#!/usr/bin/env python3
"""Calculates the average PHRED score per base for a fastq file"""
import copy
import sys
import os
import argparse
import collections
import time
import threading
import random
import multiprocessing as mp
from multiprocessing import managers
import socket
import queue
import pickle

__author__ = "Robert Warmerdam"


class FastqFileProcessingNode:
    def __init__(self, fastq_file_path, peers, host_name, port, n_local_processes=1, chunks=0):
        self.host_name = host_name
        self.port = port
        self.peers = peers
        self.authkey = b'distributed_fastq_quality_scoring'
        self.lookup_manager = make_server_manager(self.port, self.authkey)
        self.peer_client_managers = self.make_peer_client_managers()
        self.n_local_processes = n_local_processes
        self.fastq_file_path = fastq_file_path
        self.sum_of_quality_scores_per_base = collections.Counter()
        self.total_bases_per_position = collections.Counter()
        self.quality_scores_per_base = dict()
        self.jobs = set()
        self.n_chunks = (len(self.peers) + 1) * n_local_processes
        if chunks:
            self.n_chunks = chunks

    def process_fastq_file(self):
        """
        Processes a fastq file: average PHRED scores per base number are calculated
        """
        # Prepare processes
        self.prepare_jobs()
        # Access queue that is accessible by all nodes

        jobs_picked = self.lookup_manager.get_job_queue()
        shared_results_queue = self.lookup_manager.get_result_queue()
        self.connect_peer_client_managers()

        # Do jobs while they are available
        while self.is_job_available(jobs_picked):
            try:
                print("i'm trying")
                starting_byte, ending_byte = self.pick_job(jobs_picked)
            except IndexError:
                break
            print("BUSY", starting_byte, ending_byte)
            # Do job
            my_file_processor = FastqFilePartProcessor(self.fastq_file_path)
            my_file_processor.process_fastq_part(starting_byte=starting_byte,
                                                 ending_byte=ending_byte,
                                                 sum_of_quality_scores_per_base=self.sum_of_quality_scores_per_base,
                                                 total_bases_per_position=self.total_bases_per_position)

        # No more jobs are available
        # Put results
        shared_results_queue.put((self.sum_of_quality_scores_per_base, self.total_bases_per_position))

        succeeded = self.push_results(shared_results_queue)
        # Are all jobs finished?
        print(succeeded)
        if succeeded:
            return

        # Combine scores
        self.combine_results(shared_results_queue)
        # Calculate output
        self.calculate_output()

    def prepare_jobs(self):
        """
        Prepares the different jobs for reading a file.
        The different jobs get a starting byte, an ending byte and the arguments for storing end results
        :param shared_job_q: the job queue shared among workers
        :param shared_result_q: the result queue shared among workers
        """
        # Get size of fastq_file in bytes
        size = os.path.getsize(self.fastq_file_path)
        # Get the size for each daughter process by dividing
        # the total size by the number of processes
        size_per_daughter_process = size / self.n_chunks
        # Initialize starting and ending byte
        starting_byte = 0
        ending_byte = size_per_daughter_process
        for i in range(self.n_chunks):
            # Initialize process with rounded byte integers
            self.jobs.add((round(starting_byte), round(ending_byte)))
            # For next processes: update starting and ending byte
            starting_byte = ending_byte
            ending_byte += size_per_daughter_process

    def calculate_output(self):
        """
        Calculate the output of the processing step by dividing the sum of quality scores per base by the number of
        bases for that base number
        """
        for i, n_bases in self.total_bases_per_position.items():
            self.quality_scores_per_base[i] = self.sum_of_quality_scores_per_base[i] / n_bases

    def write_output(self, opened_output_file):
        """
        Write the output to a file
        :param opened_output_file: an opened output file
        """
        opened_output_file.write("base_nr, average_PHRED\n")
        for base_nr, score in self.quality_scores_per_base.items():
            opened_output_file.write("{}, {}\n".format(base_nr + 1, score))

    def pick_job(self, picked):
        choice = None
        while not choice:
            random_ = random.random() * 2
            print("sleeping", random_)
            time.sleep(random_)
            try:
                candid = random.choice(list(self.jobs))
                print("PUTTING AND CHECKING", candid)
                picked.put(candid)
                if self.is_job_choice_available(candid):
                    self.jobs.remove(candid)
                    choice = candid
            except (ConnectionRefusedError, EOFError) as e:
                print(str(e))
                print(picked.get())
        return choice

    def is_job_available(self, picked):
        return len(self.jobs) > 0

    def combine_results(self, shared_results_queue):
        """
        Empty the queue with results
        :param shared_results_queue: A queue instance containing quality lines from reads
        """
        # Reset counters
        self.sum_of_quality_scores_per_base = collections.Counter()
        self.total_bases_per_position = collections.Counter()
        # Loop through every queue element
        while not shared_results_queue.empty():
            try:
                out_tuple = shared_results_queue.get()
                sum_of_quality_scores_per_base, total_bases_per_position = out_tuple
                self.sum_of_quality_scores_per_base += sum_of_quality_scores_per_base
                self.total_bases_per_position += total_bases_per_position
            except queue.Empty:
                # if the queue suddenly throws an exception: try again
                pass

    def is_job_choice_available(self, choice):
        """
        Returns if the job of choice is still available
        :param choice:
        :return:
        """
        available = True
        for peer_connection in self.peer_client_managers:
            print("communicating job choice")
            peer_picked_jobs = peer_connection.get_job_queue()
            peer_picked_jobs.put('STOP')
            for picked_job in iter(peer_picked_jobs.get, 'STOP'):
                peer_picked_jobs.put(picked_job)
                # Remove the job from the set of possible future jobs
                if picked_job in self.jobs:
                    self.jobs.remove(picked_job)
                # Retry if the job of choice was already chosen
                if choice == picked_job:
                    available = False
            if not available:
                break
        return available

    def make_peer_client_managers(self):
        """
        Makes peer clients
        :return:
        """
        return [make_client_manager(peer, self.port, self.authkey) for peer in self.peers]

    def connect_peer_client_managers(self):
        """
        Connects peer client managers to different lookup managers in peers
        """
        for peer_client_manager in self.peer_client_managers:
            while True:
                try:
                    peer_client_manager.connect()
                    break
                except ConnectionRefusedError as e:
                    pass
            print('Client connected to {}'.format(str(peer_client_manager.address)))

    def push_results(self, shared_results_queue):
        """
        shuffle results around until no remain for this node or return False in which case this node has to fix
        the results to an output file
        :param shared_results_queue:
        :return:
        """
        succeeded = False
        for peer_connection in self.peer_client_managers:
            try:
                results_queue = peer_connection.get_result_queue()
                # Define timeout
                timeout = time.time() + 10
                time.sleep(1)
                is_timeout = False
                # Try pushing all items to an available peer
                while not shared_results_queue.empty():
                    try:
                        result = shared_results_queue.get()
                        results_queue.put(result)
                    except queue.Empty:
                        # if the queue suddenly throws an exception: try again
                        pass
                    finally:
                        if time.time() > timeout:
                            # Timeout
                            # sleep and accept having to clean up
                            time.sleep(5)
                            is_timeout = True
                            break
                if is_timeout:
                    break
                succeeded = True
            except ConnectionRefusedError:
                continue
        return succeeded


class FastqFilePartProcessor:
    def __init__(self, fastq_file_path):
        self.fastq_file_path = fastq_file_path

    def process_fastq_part(self, starting_byte, ending_byte, sum_of_quality_scores_per_base, total_bases_per_position):
        """
        Process a part of a fastq file
        The total score per base number is calculated for this part as well as the total number of bases for each
        base number
        :param starting_byte: The byte where the process will start
        :param ending_byte: The byte where the process doing the next chunk will start (or end of file)
        :param sum_of_quality_scores_per_base: The total score per base number
        :param total_bases_per_position: The total number of bases for each base number
        """
        print(starting_byte, ending_byte)
        # Open the fastq_file
        with open(self.fastq_file_path) as opened_fastq_file:
            # Go te the right location in the file using seek (unit is byte)
            opened_fastq_file.seek(starting_byte)
            # Finish the current line
            byte_position_upstream_of_seq_id_line = starting_byte
            line = opened_fastq_file.readline()
            # While the line is not a sequence identifier, read another line
            while not self.is_line_sequence_identifier(line):
                byte_position_upstream_of_seq_id_line = opened_fastq_file.tell()
                line = opened_fastq_file.readline()
            # Return to the beginning of the seq id line
            opened_fastq_file.seek(byte_position_upstream_of_seq_id_line)
            # Read reads until the read has finished that ends after the ending byte
            while opened_fastq_file.tell() < ending_byte:
                # Read a read
                self.read_a_read(opened_fastq_file, sum_of_quality_scores_per_base, total_bases_per_position)

    def read_a_read(self, opened_fastq_file, sum_of_quality_scores_per_base, total_bases_per_position):
        """
        Process a single read by adding scores to the total quality score of each base number and counting the total
        bases per base number
        :param opened_fastq_file: an opened fastq file
        :param sum_of_quality_scores_per_base: a list containing quality scores in integers
        :param total_bases_per_position: a list containing the count of bases in integers
        """
        # Read seq_id line
        opened_fastq_file.readline()
        # Read sequence line (newline present)
        sequence_line = opened_fastq_file.readline()
        # Read strand line
        opened_fastq_file.readline()
        # Read quality line (newline present)
        quality_line = opened_fastq_file.readline()
        # Check if the read is valid (sequence_line length equal to quality line length)
        if not self.is_valid_read(sequence_line, quality_line):
            # Forget about the read if it is messed up
            return
        # Strip newline characters
        quality_line = quality_line.rstrip()
        # Process the quality line by adding the score for each base nr
        # and add 1 to the total bases list for each present base nr
        for i, char in enumerate(quality_line):
            sum_of_quality_scores_per_base[i] += (ord(char) - 33)
            total_bases_per_position[i] += 1

    @staticmethod
    def is_valid_read(sequence_line, quality_line):

        """
        Determines if the read is valid
        The length of the sequence and the quality line should be equal and above zero
        :param sequence_line: A string containing the line of a sequence with newline character
        :param quality_line: A string containing the line with quality characters with newline character
        :return: True if the read is valid
        """
        # length of quality(3)/seq(1) should match
        return len(sequence_line) == len(quality_line) and len(quality_line) > 0

    @staticmethod
    def is_line_sequence_identifier(line):
        """
        Checks if the line is a seq id line
        :param line: A possible seq id line
        :return: True if the line is a seq id line
        """
        return line.startswith('@') and ' ' in line


class ArgumentParser:
    @classmethod
    def parse(cls, argv):
        """Parses commandline arguments"""
        parser = argparse.ArgumentParser(description=__doc__)
        parser.add_argument('fastq_file', type=cls.test_fastq_file, help="The path to an 'Illumina 1.3' FASTQ file")
        parser.add_argument('-n', '--number_of_chunks', required=False, type=int, default=0,
                            help="The number of chunks")
        parser.add_argument('output_file', nargs='?',
                            help="The path to a CSV output file")
        parser.add_argument('--number_of_nodes', type=int, required=True,
                            help="The number of nodes/clients to read the file simultaneously")
        parser.add_argument('--port', type=int, required=False, default=52013,
                            help="The port to use for communication")
        args = parser.parse_args(argv)
        return args

    @classmethod
    def test_fastq_file(cls, fastq_file):
        """Raises an error if the fastq_file is not readable"""
        if not os.path.isfile(fastq_file):
            raise argparse.ArgumentTypeError('fastq file {} is not a file'.format(fastq_file))
        if not os.access(fastq_file, os.R_OK):
            raise argparse.ArgumentTypeError('fastq file {} is not readable file'.format(fastq_file))
        return fastq_file


class NodeListener(threading.Thread):
    def __init__(self, number_of_nodes, host_name, host='', port=52013, ENCODING='utf-8'):
        threading.Thread.__init__(self, name="listener")
        self.host = host
        self.host_name = host_name
        self.port = port
        self.number_of_nodes = number_of_nodes
        self.encoding = ENCODING
        self.nodes = set()

    def listen(self):
        # UDP server responds to broadcast packets
        # you can have more than one instance of these running

        address = (self.host, self.port)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind(address)

            while len(self.nodes) < self.number_of_nodes:
                print("Listening")
                received_data, addr = s.recvfrom(2048)
                self.nodes.add(received_data.decode(self.encoding))
                print(addr, ':', str(received_data))
                s.sendto(b'received', addr)

    def run(self):
        self.listen()


class Broadcaster(threading.Thread):
    def __init__(self, number_of_nodes, host_name, host='<broadcast>', port=52013, ENCODING='utf-8'):
        threading.Thread.__init__(self, name="broadcaster")
        self.host_name = host_name
        self.host = host
        self.port = port
        self.encoding = ENCODING
        self.number_of_nodes = number_of_nodes

    def broadcast(self):
        # UDP client broadcasts to server(s)

        address = (self.host, self.port)
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
            s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

            data = self.host_name.encode(self.encoding)
            addresses = set()
            while len(addresses) < self.number_of_nodes:
                time.sleep(random.random() * 2)
                s.sendto(data, address)
                received_data, addr = s.recvfrom(2048)
                addresses.add(addr)

    def run(self):
        self.broadcast()


def make_client_manager(ip, port, authkey):
    """
    Create a manager for a client. This manager connects to a server on the
    given address and exposes the get_job_q and get_result_q methods for
    accessing the shared queues from the server.
    Return a manager object.
    """

    class ServerQueueManager(managers.SyncManager):
        pass

    ServerQueueManager.register('get_job_queue')
    ServerQueueManager.register('get_result_queue')

    manager = ServerQueueManager(address=(ip, port), authkey=authkey)
    return manager


def make_server_manager(port, authkey):
    """
    Create a manager for the server, listening on the given port.
    Return a manager object with get_job_q and get_result_q methods.
    """
    job_queue = mp.Queue()
    result_queue = mp.Queue()

    # This is based on the examples in the official docs of multiprocessing.
    # get_{job|result}_q return synchronized proxies for the actual Queue
    # objects.
    class JobQueueManager(managers.SyncManager):
        pass

    JobQueueManager.register('get_job_queue', callable=lambda: job_queue)
    JobQueueManager.register('get_result_queue', callable=lambda: result_queue)

    manager = JobQueueManager(address=('', port), authkey=authkey)
    manager.start()
    print('Server started at port {}'.format(port))
    return manager


def get_peers(number_of_nodes, host_name):
    listener = NodeListener(number_of_nodes, host_name)
    listener.start()
    broadcaster = Broadcaster(number_of_nodes, host_name)
    broadcaster.start()
    listener.join()
    broadcaster.join()
    peers = listener.nodes
    peers.remove(host_name)
    return listener.nodes


def main(argv=None):
    if argv is None:
        argv = sys.argv
    args = ArgumentParser.parse(argv[1:])
    # Parse Fastq file
    host_name = socket.gethostname()
    nodelist = get_peers(args.number_of_nodes, host_name)
    my_node = FastqFileProcessingNode(fastq_file_path=args.fastq_file,
                                      peers=nodelist,
                                      host_name=host_name,
                                      port=args.port,
                                      chunks=args.number_of_chunks)
    my_node.process_fastq_file()
    if args.output_file is not None:
        with open(args.output_file, 'w') as opened_output_file:
            my_node.write_output(opened_output_file)
    else:
        my_node.write_output(sys.stdout)
    return 0


if __name__ == "__main__":
    sys.exit(main())
