#!/bin/bash
#SBATCH --time 0:20:00
#SBATCH --nodes=4
#SBATCH --cpus-per-task=1
#SBATCH --mem=4096
#SBATCH --partition=short
#SBATCH --output=out.txt

module load Python/3.6.4-foss-2018a

/usr/bin/env python3 script.py -n 42 -number_of_nodes 4 ../rnaseq.fastq out.csv
